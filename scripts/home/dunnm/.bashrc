PATH=/usr/local/bin:$PATH

alias ll='ls -l'
alias la='ls -lAF'
if ! hash md5sum 2>/dev/null; then
	alias md5sum='md5 -r'
fi
alias grep='grep --color'
alias gitk='gitk --all'
alias ie="wine 'C:\Program Files\Internet Explorer\iexplore'"
alias tree='tree -C'
if ! hash ifconfig 2>/dev/null; then
	alias ifconfig='ipconfig'
fi
alias chrome_insecure='/cygdrive/c/Users/vm/AppData/Local/Google/Chrome/Application/chrome.exe --ignore-certificate-errors --user-data-dir=/tmp/temporaryprofiledirectory'
